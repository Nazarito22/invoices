mockApp.config(['$routeProvider','$locationProvider','$resourceProvider',
    function config($routeProvide,$locationProvider,$resourceProvider) {
        $locationProvider.html5Mode({
            enabled: true,
            requireBase: false
        });

        $routeProvide.
            when('/products', {
                template: '<products-component></products-component>'
            }).
            when('/invoices', {
                template: '<invoices-component></invoices-component>'
            }).
            when('/invoices/:id', {
                templateUrl: '/app/templates/invoices-item.template.html',
                controller: 'InvoicesItemCtrl'
            }).
            when('/customers', {
                template: '<customers-component></customers-component>'
            }).
            otherwise({
                redirectTo: '/invoices'
            });
        $resourceProvider.defaults.stripTrailingSlashes = false;
    }
]);
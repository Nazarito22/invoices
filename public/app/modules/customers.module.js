var customers = angular.module('customers', ['ngRoute']);

customers.factory('customersFactory', function ($http) {

    var factory = {};

    factory.getItems = function () {
        return $http.get('/api/customers');
    };

    factory.addItem = function (customer) {
        return $http.post('/api/customers/', customer);
    };

    factory.deleteItem = function (data) {
        return $http.delete('/api/customers/' + data.id);
    };

    return factory;
});

customers.component('customersComponent', {
    controller: function (customersFactory) {

        var vm = this;

        customersFactory.getItems().then(function(response){
            vm.customers = response.data;
        });

        vm.addItem = function addItem(customer, customersForm) {
            if (customersForm.$valid) {
                customersFactory.addItem(customer).then(function (response) {
                    vm.customers.push(response.data);
                });
            }
        };

        vm.removeItem = function removeItem(data) {
            customersFactory.deleteItem(data).then(function () {
                var index = vm.customers.indexOf(data);
                vm.customers.splice(index, 1);
            });
        };
    },
    templateUrl: '/app/templates/customers.template.html'
});